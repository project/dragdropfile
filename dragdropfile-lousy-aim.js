
jQuery(function($) {

  $(document).bind({
    dragover: function(e) {
      if (!$(e.target).closest('.dragdropfile-processed').length) {
        e.originalEvent.dataTransfer.dropEffect = 'none';
      }

      e.preventDefault();
    }, // dragover

    dragleave: function(e) {
      e.preventDefault();
    }, // dragleave

    drop: function(e) {
      if ('INPUT' != e.target.nodeName) {
        e.preventDefault();
      }
    } // drop
  });

});
